clean:;
	mvn clean
compile-java: validate-spec generate-spec;
	mvn compile package
compile-java-quick: validate-spec;
	mvn compile package -Dmaven.test.skip=true
compile: clean compile-java;
compile-quick: clean compile-java-quick;
install-yarn:;
	yarn install --frozen-lockfile --audit
generate-spec: install-yarn;
	yarn run generate-json-schema
validate-spec: install-yarn;
compile-start: compile-quick;
	docker-compose down
	docker-compose build
	docker-compose up
compile-start-headless: compile-quick;
	docker-compose down
	docker-compose build
	docker-compose up -d
start-spec: validate-spec;
	yarn run start
generate-notice-src:;
	docker run -v $(PWD)/:/project \
		-v /project/node_modules \
		-v /project/volumes \
		scancode-toolkit -clpeui --json-pp /project/result.json /project