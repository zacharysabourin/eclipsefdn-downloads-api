CREATE TABLE downloads (
  file_id int(10) unsigned NOT NULL DEFAULT 0,
  download_date datetime DEFAULT NULL,
  remote_host varchar(100) DEFAULT NULL,
  remote_addr varchar(15) DEFAULT NULL,
  mirror_id int(10) unsigned DEFAULT NULL,
  ccode char(2) DEFAULT NULL
);
INSERT INTO downloads(file_id, download_date, remote_host, remote_addr,mirror_id, ccode)
  VALUES (1, NOW(),'http://somehost.co/file/location', '127.0.0.1', 101, 'ca');

CREATE TABLE download_file_index (
  file_id int(10) unsigned NOT NULL AUTO_INCREMENT,
  file_name varchar(255) NOT NULL DEFAULT '',
  download_count int(10) unsigned NOT NULL DEFAULT 0,
  size_disk_bytes bigint(20) NOT NULL DEFAULT 0,
  timestamp_disk bigint(20) NOT NULL DEFAULT 0,
  md5sum char(32) DEFAULT NULL,
  sha1sum char(40) DEFAULT NULL
);
INSERT INTO download_file_index(file_id, file_name, download_count, size_disk_bytes, timestamp_disk, md5sum, sha1sum)
  VALUES (1, 'sample_file.zip',123456, 987654, 1642175700000, 'md5sum_sample', 'sha1sum_sample');