package org.eclipsefoundation.downloads.test.helper;

public final class SchemaNamespaceHelper {
    public static final String BASE_SCHEMAS_PATH = "schemas/";
    public static final String BASE_SCHEMAS_PATH_SUFFIX = "-schema.json";
    public static final String FILES_SCHEMA_PATH = BASE_SCHEMAS_PATH + "files" + BASE_SCHEMAS_PATH_SUFFIX;
    public static final String FILE_SCHEMA_PATH = BASE_SCHEMAS_PATH + "file" + BASE_SCHEMAS_PATH_SUFFIX;
    public static final String RELEASES_SCHEMA_PATH = BASE_SCHEMAS_PATH + "releases" + BASE_SCHEMAS_PATH_SUFFIX;
    public static final String RELEASE_SCHEMA_PATH = BASE_SCHEMAS_PATH + "release" + BASE_SCHEMAS_PATH_SUFFIX;
}
