package org.eclipsefoundation.downloads.resources;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

import javax.inject.Inject;

import org.eclipsefoundation.downloads.namespaces.DownloadsUrlParameterNames;
import org.eclipsefoundation.downloads.test.helper.SchemaNamespaceHelper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.quarkus.test.junit.QuarkusTest;

@TestInstance(Lifecycle.PER_CLASS)
@QuarkusTest
public class DownloadsResourceTest {
    public static final String DOWNLOADS_BASE_URL = "/downloads";
    public static final String FILE_BY_ID_URL = DOWNLOADS_BASE_URL + "/file/{id}";

    public static final String RELEASE_BASE_URL = DOWNLOADS_BASE_URL + "/releases";
    public static final String LEGACY_RELEASE_URL = DOWNLOADS_BASE_URL + "/release/{releaseType}";
    public static final String RELEASES_URL = RELEASE_BASE_URL + "/{releaseName}";
    public static final String RELEASE_VERSION_URL = RELEASES_URL + "/{releaseVersion}";

    @Inject
    ObjectMapper json;

    @Test
    void getFileByID_missingFile() {
        given().when().get(FILE_BY_ID_URL, "99999").then().statusCode(404);
    }

    @Test
    void getFileByID_success() {
        given().when().get(FILE_BY_ID_URL, "1").then().statusCode(200);
    }

    @Test
    void getFileByID_success_format() {
        given().when().get(FILE_BY_ID_URL, "1").then().assertThat()
                .body(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.FILE_SCHEMA_PATH));
    }

    @Test
    void getReleaseLegacy_missingReleaseName() {
        given().when().get(LEGACY_RELEASE_URL, "epp").then().statusCode(400);
        given().when().get(LEGACY_RELEASE_URL, "eclipse_packages").then().statusCode(400);
        given().when().queryParam(DownloadsUrlParameterNames.RELEASE_VERSION_VALUE, "r").get(LEGACY_RELEASE_URL, "epp")
                .then().statusCode(400);
        given().when().queryParam(DownloadsUrlParameterNames.RELEASE_VERSION_VALUE, "r")
                .get(LEGACY_RELEASE_URL, "eclipse_packages").then().statusCode(400);
    }

    @Test
    void getReleaseLegacy_success() {
        given().when().queryParam(DownloadsUrlParameterNames.RELEASE_NAME_VALUE, "2021-12")
                .get(LEGACY_RELEASE_URL, "epp").then().statusCode(200);
        given().when().queryParam(DownloadsUrlParameterNames.RELEASE_NAME_VALUE, "2021-12")
                .get(LEGACY_RELEASE_URL, "eclipse_packages").then().statusCode(200);
        given().when().queryParam(DownloadsUrlParameterNames.RELEASE_NAME_VALUE, "2021-12")
                .queryParam(DownloadsUrlParameterNames.RELEASE_VERSION_VALUE, "r").get(LEGACY_RELEASE_URL, "epp").then()
                .statusCode(200);
        given().when().queryParam(DownloadsUrlParameterNames.RELEASE_NAME_VALUE, "2021-12")
                .queryParam(DownloadsUrlParameterNames.RELEASE_VERSION_VALUE, "r")
                .get(LEGACY_RELEASE_URL, "eclipse_packages").then().statusCode(200);
    }

    @Test
    void getReleaseLegacy_success_format() {
        given().when().queryParam(DownloadsUrlParameterNames.RELEASE_NAME_VALUE, "2021-12")
                .get(LEGACY_RELEASE_URL, "epp").then().assertThat()
                .body(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.RELEASES_SCHEMA_PATH));
    }

    @Test
    void getReleaseLegacyWithVersion_success_format() {
        given().when().queryParam(DownloadsUrlParameterNames.RELEASE_NAME_VALUE, "2021-12")
                .queryParam(DownloadsUrlParameterNames.RELEASE_VERSION_VALUE, "r").get(LEGACY_RELEASE_URL, "epp").then()
                .assertThat().body(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.RELEASE_SCHEMA_PATH));
    }

    @Test
    void getRelease_missingRelease() {
        given().when().get(RELEASES_URL, "missing-version").then().statusCode(404);
    }

    @Test
    void getRelease_success() {
        given().when().get(RELEASES_URL, "2021-12").then().statusCode(200);
    }

    @Test
    void getRelease_success_format() {
        given().when().get(RELEASES_URL, "2021-12").then().assertThat()
                .body(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.RELEASES_SCHEMA_PATH));
    }

    @Test
    void getReleaseVersion_missingRelease() {
        given().when().get(RELEASE_VERSION_URL, "missing-version", "r").then().statusCode(404);
    }

    @Test
    void getReleaseVersion_missingVersion() {
        given().when().get(RELEASE_VERSION_URL, "2021-12", "some-release").then().statusCode(404);
    }

    @Test
    void getReleaseVersion_success() {
        given().when().get(RELEASE_VERSION_URL, "2021-12", "r").then().statusCode(200);
    }

    @Test
    void getReleaseVersion_success_format() {
        given().when().get(RELEASE_VERSION_URL, "2021-12", "r").then().assertThat()
                .body(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.RELEASE_SCHEMA_PATH));
    }
}
