package org.eclipsefoundation.downloads.services.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.service.CachingService;
import org.eclipsefoundation.downloads.api.DrupalAPI;
import org.eclipsefoundation.downloads.models.TrackedReleases;
import org.eclipsefoundation.downloads.models.TrackedReleases.Release;
import org.eclipsefoundation.downloads.models.TrackedReleases.ReleaseVersion;
import org.eclipsefoundation.downloads.services.ReleaseTrackerService;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;

/**
 * Default implementation of the release tracker service, using the Drupal API as the source of data for the releases.
 * 
 * @author Martin Lowe
 *
 */
@ApplicationScoped
public class DefaultReleaseTrackerService implements ReleaseTrackerService {

    @Inject
    CachingService cache;

    @RestClient
    @Inject
    DrupalAPI api;

    @Override
    public Optional<Release> getReleaseByName(String releaseName) {
        return Optional.ofNullable(releases().getReleases().get(releaseName));
    }

    @Override
    public List<Release> getActiveReleases() {
        return releases().getReleases().values().stream()
                .filter(r -> r.getVersions().stream().anyMatch(ReleaseVersion::getIsCurrent))
                .collect(Collectors.toList());
    }

    /**
     * Get the cached releases from the default location (Drupal API).
     * 
     * @return cached version of releases, or an empty object.
     */
    private TrackedReleases releases() {
        return cache.get("all", new MultivaluedMapImpl<>(), TrackedReleases.class, api::getTrackedReleases)
                .orElse(TrackedReleases.builder().setReleases(new HashMap<>()).build());
    }
}
