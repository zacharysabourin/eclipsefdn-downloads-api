package org.eclipsefoundation.downloads.dto;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.downloads.namespaces.DownloadsUrlParameterNames;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.DtoTable;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;

@Entity
@Table(name = "download_file_index")
public class DownloadFileIndex extends BareNode {
    public static final DtoTable TABLE = new DtoTable(DownloadFileIndex.class, "dfi");

    @Id
    private int fileId;
    @NotNull
    private String fileName;
    @NotNull
    private int downloadCount;
    @NotNull
    private int sizeDiskBytes;
    @NotNull
    private long timestampDisk;
    private String md5sum;
    private String sha1sum;

    @Override
    public Object getId() {
        return getFileId();
    }

    /**
     * @return the fileId
     */
    public int getFileId() {
        return fileId;
    }

    /**
     * @param fileId the fileId to set
     */
    public void setFileId(int fileId) {
        this.fileId = fileId;
    }

    /**
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * @param fileName the fileName to set
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * @return the downloadCount
     */
    public int getDownloadCount() {
        return downloadCount;
    }

    /**
     * @param downloadCount the downloadCount to set
     */
    public void setDownloadCount(int downloadCount) {
        this.downloadCount = downloadCount;
    }

    /**
     * @return the sizeDiskBytes
     */
    public int getSizeDiskBytes() {
        return sizeDiskBytes;
    }

    /**
     * @param sizeDiskBytes the sizeDiskBytes to set
     */
    public void setSizeDiskBytes(int sizeDiskBytes) {
        this.sizeDiskBytes = sizeDiskBytes;
    }

    /**
     * @return the timestampDisk
     */
    public long getTimestampDisk() {
        return timestampDisk;
    }

    /**
     * @param timestampDisk the timestampDisk to set
     */
    public void setTimestampDisk(long timestampDisk) {
        this.timestampDisk = timestampDisk;
    }

    /**
     * @return the md5sum
     */
    public String getMd5sum() {
        return md5sum;
    }

    /**
     * @param md5sum the md5sum to set
     */
    public void setMd5sum(String md5sum) {
        this.md5sum = md5sum;
    }

    /**
     * @return the sha1sum
     */
    public String getSha1sum() {
        return sha1sum;
    }

    /**
     * @param sha1sum the sha1sum to set
     */
    public void setSha1sum(String sha1sum) {
        this.sha1sum = sha1sum;
    }

    @Singleton
    public static class DownloadFileIndexFilter implements DtoFilter<DownloadFileIndex> {
        @Inject
        ParameterizedSQLStatementBuilder builder;

        @Override
        public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
            ParameterizedSQLStatement stmt = builder.build(TABLE);
            if (isRoot) {
                // ID check
                String id = params.getFirst(DefaultUrlParameterNames.ID.getName());
                if (StringUtils.isNumeric(id)) {
                    stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".fileId = ?",
                            new Object[] { Integer.valueOf(id) }));
                }
                // file name check
                String fileName = params.getFirst(DownloadsUrlParameterNames.FILE_NAME.getName());
                if (fileName != null) {
                    stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".fileName = ?",
                            new Object[] { fileName }));
                }
            }
            return stmt;
        }

        @Override
        public Class<DownloadFileIndex> getType() {
            return DownloadFileIndex.class;
        }

    }
}
