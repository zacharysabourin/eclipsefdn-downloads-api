package org.eclipsefoundation.downloads.dto;

import java.util.Date;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.ws.rs.core.MultivaluedMap;

import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.downloads.namespaces.DownloadsUrlParameterNames;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.DtoTable;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;

@Entity
@Table(name = "downloads")
public class Downloads extends BareNode {
    public static final DtoTable TABLE = new DtoTable(Downloads.class, "d");

    @Id
    private int fileId;
    private Date downloadDate;
    private String remoteHost;
    private String remoteAddr;
    private Integer mirrorId;
    private String ccode;

    @Override
    public Object getId() {
        return getFileId();
    }
    
    /**
     * @return the fileId
     */
    public int getFileId() {
        return fileId;
    }

    /**
     * @param fileId the fileId to set
     */
    public void setFileId(int fileId) {
        this.fileId = fileId;
    }

    /**
     * @return the downloadDate
     */
    public Date getDownloadDate() {
        return downloadDate;
    }

    /**
     * @param downloadDate the downloadDate to set
     */
    public void setDownloadDate(Date downloadDate) {
        this.downloadDate = downloadDate;
    }

    /**
     * @return the remoteHost
     */
    public String getRemoteHost() {
        return remoteHost;
    }

    /**
     * @param remoteHost the remoteHost to set
     */
    public void setRemoteHost(String remoteHost) {
        this.remoteHost = remoteHost;
    }

    /**
     * @return the remoteAddr
     */
    public String getRemoteAddr() {
        return remoteAddr;
    }

    /**
     * @param remoteAddr the remoteAddr to set
     */
    public void setRemoteAddr(String remoteAddr) {
        this.remoteAddr = remoteAddr;
    }

    /**
     * @return the mirrorId
     */
    public Integer getMirrorId() {
        return mirrorId;
    }

    /**
     * @param mirrorId the mirrorId to set
     */
    public void setMirrorId(Integer mirrorId) {
        this.mirrorId = mirrorId;
    }

    /**
     * @return the ccode
     */
    public String getCcode() {
        return ccode;
    }

    /**
     * @param ccode the ccode to set
     */
    public void setCcode(String ccode) {
        this.ccode = ccode;
    }

    @Singleton
    public static class DownloadsFilter implements DtoFilter<Downloads> {
        @Inject
        ParameterizedSQLStatementBuilder builder;

        @Override
        public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
            ParameterizedSQLStatement stmt = builder.build(TABLE);
            if (isRoot) {
                // ID check
                String id = params.getFirst(DefaultUrlParameterNames.ID.getName());
                if (id != null) {
                    stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".fileId = ?",
                            new Object[] { id }));
                }
                // mirror ID check
                String mirrorId = params.getFirst(DownloadsUrlParameterNames.MIRROR_ID.getName());
                if (mirrorId != null) {
                    stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".mirrorId = ?",
                            new Object[] { mirrorId }));
                }
                // remote host check
                String remoteHost = params.getFirst(DownloadsUrlParameterNames.REMOTE_HOST.getName());
                if (remoteHost != null) {
                    stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".remoteHost = ?",
                            new Object[] { remoteHost }));
                }
                // remote address check
                String remoteAddr = params.getFirst(DownloadsUrlParameterNames.REMOTE_ADDR.getName());
                if (remoteAddr != null) {
                    stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".remoteAddr = ?",
                            new Object[] { remoteAddr }));
                }
                // ccode check
                String ccode = params.getFirst(DownloadsUrlParameterNames.CCODE.getName());
                if (ccode != null) {
                    stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".ccode = ?",
                            new Object[] { ccode }));
                }
            }
            return stmt;
        }

        @Override
        public Class<Downloads> getType() {
            return Downloads.class;
        }

    }
}
