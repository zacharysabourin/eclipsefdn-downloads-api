package org.eclipsefoundation.downloads.namespaces;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.inject.Singleton;

import org.eclipsefoundation.core.namespace.UrlParameterNamespace;

@Singleton
public final class DownloadsUrlParameterNames implements UrlParameterNamespace {
    public static final String RELEASE_NAME_VALUE = "release_name";
    public static final String RELEASE_VERSION_VALUE = "release_version";

    public static final UrlParameter MIRROR_ID = new UrlParameter("mirror_id");
    public static final UrlParameter CCODE = new UrlParameter("ccode");
    public static final UrlParameter REMOTE_ADDR = new UrlParameter("remote_addr");
    public static final UrlParameter REMOTE_HOST = new UrlParameter("remote_host");
    public static final UrlParameter FILE_NAME = new UrlParameter("file_name");
    public static final UrlParameter RELEASE_NAME = new UrlParameter(RELEASE_NAME_VALUE);
    public static final UrlParameter RELEASE_VERSION = new UrlParameter(RELEASE_VERSION_VALUE);

    private static final List<UrlParameter> params = Collections.unmodifiableList(
            Arrays.asList(MIRROR_ID, CCODE, REMOTE_ADDR, REMOTE_HOST, FILE_NAME, RELEASE_NAME, RELEASE_VERSION));

    @Override
    public List<UrlParameter> getParameters() {
        return new ArrayList<>(params);
    }
}