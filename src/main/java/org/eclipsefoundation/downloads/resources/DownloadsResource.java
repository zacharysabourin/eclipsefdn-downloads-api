package org.eclipsefoundation.downloads.resources;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.model.Error;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.core.service.CachingService;
import org.eclipsefoundation.downloads.api.DrupalAPI;
import org.eclipsefoundation.downloads.dto.DownloadFileIndex;
import org.eclipsefoundation.downloads.models.ReleaseVersionPackages;
import org.eclipsefoundation.downloads.models.ReleaseVersionPackages.ReleaseTrackerPackages;
import org.eclipsefoundation.downloads.models.TrackedReleases.Release;
import org.eclipsefoundation.downloads.namespaces.DownloadsUrlParameterNames;
import org.eclipsefoundation.downloads.services.ReleaseTrackerService;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/downloads")
@Produces(MediaType.APPLICATION_JSON)
public class DownloadsResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(DownloadsResource.class);

    @Inject
    DefaultHibernateDao dao;
    @Inject
    FilterService filters;
    @Inject
    CachingService cache;
    @Inject
    RequestWrapper wrap;

    @Inject
    @RestClient
    DrupalAPI api;

    @Inject
    ReleaseTrackerService releaseTrackerService;

    @GET
    @Path("file")
    public Response all() {
        return Response.ok(cache
                .get("all", new MultivaluedMapImpl<>(), DownloadFileIndex.class,
                        () -> dao.get(new RDBMSQuery<>(wrap, filters.get(DownloadFileIndex.class))))
                .orElseGet(Collections::emptyList)).build();
    }

    @GET
    @Path("file/{id}")
    public Response byID(@PathParam("id") String id) {
        // filter by ID for the file index
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(DefaultUrlParameterNames.ID.getName(), id);
        // get the index, and make sure we have a result
        Optional<List<DownloadFileIndex>> dfis = cache.get(id, params, DownloadFileIndex.class,
                () -> dao.get(new RDBMSQuery<>(wrap, filters.get(DownloadFileIndex.class), params)));
        if (dfis.isEmpty() || dfis.get().isEmpty()) {
            String message = String.format("No DownloadFileIndex found with id '%s'", id);
            LOGGER.debug(message);
            return new Error(404, message).asResponse();
        }
        return Response.ok(dfis.get().get(0)).build();
    }

    @GET
    @Path("release/{type:(epp|eclipse_packages)}")
    public Response packageRelease(@QueryParam(DownloadsUrlParameterNames.RELEASE_NAME_VALUE) String releaseName,
            @QueryParam(DownloadsUrlParameterNames.RELEASE_VERSION_VALUE) String releaseVersion) {
        // use existing calls and route according to what query params are provided
        if (StringUtils.isNotBlank(releaseName) && StringUtils.isNotBlank(releaseVersion)) {
            return releaseVersion(releaseName, releaseVersion);
        } else if (StringUtils.isNotBlank(releaseName)) {
            return release(releaseName);
        }
        return new Error(400, "At least the release_name query parameter is required to use this endpoint")
                .asResponse();
    }

    @GET
    @Path("releases/active")
    public Response activeReleases() {
        return Response.ok(releaseTrackerService.getActiveReleases()).build();
    }

    @GET
    @Path("releases/{releaseName}")
    public Response release(@PathParam("releaseName") String releaseName) {
        // check that the release name is valid
        Optional<Release> r = releaseTrackerService.getReleaseByName(releaseName);
        if (r.isEmpty()) {
            String message = String.format("No release named '%s' found in store, cannot continue", releaseName);
            LOGGER.debug(message);
            return new Error(404, message).asResponse();
        }
        // get all of the release version packages to return
        try {
            return Response.ok(r.get().getVersions().stream()
                    .map(v -> ReleaseVersionPackages.builder().setPackages(api.get(releaseName, v.getName()))
                            .setReleaseName(releaseName).setReleaseVersion(v.getName()).build())
                    .collect(Collectors.toList())).build();
        } catch (WebApplicationException e) {
            String message = String.format("Error while retrieving versioned packages for release '%s': %s",
                    releaseName, e.getLocalizedMessage());
            LOGGER.error(message);
            return new Error(503, message).asResponse();
        }
    }

    @GET
    @Path("releases/{releaseName}/{version}")
    public Response releaseVersion(@PathParam("releaseName") String releaseName, @PathParam("version") String version) {
        // check that the release name is valid
        Optional<Release> r = releaseTrackerService.getReleaseByName(releaseName);
        if (r.isEmpty()) {
            String message = String.format("No release named '%s' found in store, cannot continue", releaseName);
            LOGGER.debug(message);
            return new Error(404, message).asResponse();
        }
        // check if there is a release version that matches the given version
        if (r.get().getVersions().stream().noneMatch(rp -> rp.getName().equals(version))) {
            String message = String.format("No version named '%s' found for release named '%s'", version, releaseName);
            LOGGER.debug(message);
            return new Error(404, message).asResponse();

        }
        // get the release package from the Drupal API
        Optional<ReleaseTrackerPackages> rtp = getVersionPackages(releaseName, version);
        if (rtp.isEmpty()) {
            String message = String.format("Could not find package definitions for release '%s', version '%s'",
                    releaseName, version);
            LOGGER.error(message);
            return new Error(503, message).asResponse();
        }
        return Response.ok(ReleaseVersionPackages.builder().setPackages(rtp.get()).setReleaseName(releaseName)
                .setReleaseVersion(version).build()).build();
    }

    /**
     * Get a cached version of the release packages from the API.
     * 
     * @param releaseName the name of the release
     * @param version the version name of the specific release
     * @return
     */
    private Optional<ReleaseTrackerPackages> getVersionPackages(String releaseName, String version) {
        return cache.get(releaseName + version, new MultivaluedMapImpl<>(), ReleaseTrackerPackages.class,
                () -> api.get(releaseName, version));
    }
}