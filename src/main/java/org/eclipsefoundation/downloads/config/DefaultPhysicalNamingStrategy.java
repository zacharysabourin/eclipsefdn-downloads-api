package org.eclipsefoundation.downloads.config;

import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.boot.model.naming.PhysicalNamingStrategy;
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment;

import com.google.common.base.CaseFormat;

public class DefaultPhysicalNamingStrategy implements PhysicalNamingStrategy {

    @Override
    public Identifier toPhysicalCatalogName(Identifier name, JdbcEnvironment jdbcEnvironment) {
        return convert(CaseFormat.LOWER_CAMEL, CaseFormat.LOWER_UNDERSCORE, name);
    }

    @Override
    public Identifier toPhysicalSchemaName(Identifier name, JdbcEnvironment jdbcEnvironment) {
        return convert(CaseFormat.LOWER_CAMEL, CaseFormat.LOWER_UNDERSCORE, name);
    }

    @Override
    public Identifier toPhysicalTableName(Identifier name, JdbcEnvironment jdbcEnvironment) {
        return convert(CaseFormat.LOWER_CAMEL, CaseFormat.LOWER_UNDERSCORE, name);
    }

    @Override
    public Identifier toPhysicalSequenceName(Identifier name, JdbcEnvironment jdbcEnvironment) {
        return convert(CaseFormat.LOWER_CAMEL, CaseFormat.LOWER_UNDERSCORE, name);
    }

    @Override
    public Identifier toPhysicalColumnName(Identifier name, JdbcEnvironment jdbcEnvironment) {
        return convert(CaseFormat.LOWER_CAMEL, CaseFormat.LOWER_UNDERSCORE, name);
    }

    private Identifier convert(CaseFormat originFormat, CaseFormat outputFormat, Identifier source) {
        if (source == null || source.getText() == null) {
            return source;
        }
        return new Identifier(originFormat.to(outputFormat, source.getText()), source.isQuoted());
    }
}
