package org.eclipsefoundation.downloads.api;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipsefoundation.downloads.models.ReleaseVersionPackages.ReleaseTrackerPackages;
import org.eclipsefoundation.downloads.models.TrackedReleases;
import org.jboss.resteasy.annotations.GZIP;

@GZIP
@RegisterRestClient(configKey = "drupal")
public interface DrupalAPI {

    @GET
    @Path("downloads/packages/admin/release_tracker/json/")
    TrackedReleases getTrackedReleases();

    @GET
    @Path("downloads/packages/admin/release_tracker/json/{releaseName}%20{version}/all")
    ReleaseTrackerPackages get(@PathParam("releaseName") String releaseName, @PathParam("version") String version);
}
