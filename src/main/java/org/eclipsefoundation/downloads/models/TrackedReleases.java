package org.eclipsefoundation.downloads.models;

import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * JSON model class for Drupal API return for tracked releases.
 * 
 * @author Martin Lowe
 *
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_TrackedReleases.Builder.class)
public abstract class TrackedReleases {

    public abstract Map<String, Release> getReleases();

    public static Builder builder() {
        return new AutoValue_TrackedReleases.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setReleases(Map<String, Release> releases);

        public abstract TrackedReleases build();
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_TrackedReleases_Release.Builder.class)
    public abstract static class Release {
        public abstract String getName();

        public abstract List<ReleaseVersion> getVersions();

        public static Builder builder() {
            return new AutoValue_TrackedReleases_Release.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {
            public abstract Builder setName(String name);

            public abstract Builder setVersions(List<ReleaseVersion> version);

            public abstract Release build();
        }
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_TrackedReleases_ReleaseVersion.Builder.class)
    public abstract static class ReleaseVersion {
        public abstract String getName();

        public abstract String getType();

        public abstract String getUrl();

        @Nullable
        public abstract Boolean getIsCurrent();

        public static Builder builder() {
            return new AutoValue_TrackedReleases_ReleaseVersion.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {
            public abstract Builder setName(String name);

            public abstract Builder setType(String type);

            public abstract Builder setUrl(String url);

            public abstract Builder setIsCurrent(@Nullable Boolean isCurrent);

            public abstract ReleaseVersion build();
        }
    }
}
