# eclipsefdn-downloads-api

## Starting a development environment

Requirements:  

* Docker  
* docker-compose  
* Java 11 or greater  
* mvn 
* node.js + npm  

Recommended:  

* make  

Before starting, an environment file should be created to ensure that variables are properly available to the docker-compose instance that will hold the development environment. A copy of the base variables and some sample values are available under `./config/.env.sample`, and a copy for this workspace can be created by copying this file to the project root under the name `.env`. It is recommended that the base values for passwords be updated to ensure better security for the given system.

Once created, a copy of the `sample.secret.properties` file in the `./config` folder should be created in the same folder with the name `secret.properties`. Once created, if the `.env` file was updated from the base, the `quarkus.datasource.password` should be updated to reflect the value for `DOWNLOADS_MYSQL_PASSWORD`.

After both the `.env` and the `./config/secret.properties` files have been created and configured, run either `make compile-start` or `make compile-start-headless`. The first option binds the docker containers to the current terminal, and ends the processes when the terminal is closed. The second option starts the docker containers as part of the daemon and persists beyond the terminal session.

The application can be checked by visiting http://localhost:8090/downloads/release/2022-03. At this address, a return should be visible that represents the 2022-03 release of the Eclipse IDE. 

## Updating notice

Notice file contents were authored using the output of the Scancode-toolkit, running against the local environment. Any new packages added to the project should be evaluated before they are added to the project to ensure they are compatible with the EPL-2.0 license that this API is available under. To make use of Scancode, it will need to be [installed for Docker](https://scancode-toolkit.readthedocs.io/en/latest/getting-started/install.html#installation-via-docker), and its recommended that the [Scancode-Workbench](https://github.com/nexB/scancode-workbench) be used to evaluate the results.