CREATE TABLE `downloads` (
  `file_id` int(10) unsigned NOT NULL DEFAULT 0,
  `download_date` datetime DEFAULT NULL,
  `remote_host` varchar(100) DEFAULT NULL,
  `remote_addr` varchar(15) DEFAULT NULL,
  `mirror_id` int(10) unsigned DEFAULT NULL,
  `ccode` char(2) DEFAULT NULL,
  KEY `IDX_file_id` (`file_id`),
  KEY `IDX_download_date` (`download_date`),
  KEY `IDX_ccode` (`ccode`),
  KEY `IDX_mirror_id` (`mirror_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 MAX_ROWS=1000000000 AVG_ROW_LENGTH=56;

CREATE TABLE `download_file_index` (
  `file_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `file_name` varchar(255) NOT NULL DEFAULT '',
  `download_count` int(10) unsigned NOT NULL DEFAULT 0,
  `timestamp_disk` bigint(20) NOT NULL DEFAULT 0,
  `md5sum` char(32) DEFAULT NULL,
  `sha1sum` char(40) DEFAULT NULL,
  PRIMARY KEY (`file_id`),
  KEY `IDX_timestamp_disk` (`timestamp_disk`),
  KEY `idx_file_name` (`file_name`(30))
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;